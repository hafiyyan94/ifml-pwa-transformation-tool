from main.utils.ast.base import Node
from main.utils.jinja.angular import base_file_writer, app_components_writer

class NotLoginTypescriptClass(Node):

    def __init__(self, url, domain_name, client_id, client_secret):
        super().__init__()
        self.selector_name = 'error-page-not-login'
        self.class_name = 'ErrorPageNotLogin'
        self.url = url
        self.domain_name = domain_name
        self.client_id = client_id
        self.client_secret = client_secret

    def render(self):
        return app_components_writer('error-page-not-login/error-page-not-login.component.ts.template',url = self.url, domain_name = self.domain_name, client_id = self.client_id, client_secret = self.client_secret)

    def get_class_name(self):
        return self.class_name

class NotLoginHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return base_file_writer('src/app/error-page-not-login/error-page-not-login.component.html.template')