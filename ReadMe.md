# IFML to Angular Generator
This tool generates AngularJS source code for web applications. The input is an abstract UI,
modeled with Interaction Flow Modeling Language (IFML). 


# Installation
The following environments are required to run IFML to Angular Generator:
1. Eclipse with IFML Editor. Check https://ifml.github.io/ for installation details.
2. Python, min version 3.5. Download: https://www.python.org/downloads/
3. NodeJS, min version 10.16.0. Download: https://nodejs.org/en/

The source code of IFML to Angular generator is available on Gitlab repository: https://gitlab.com/RSE-Lab-FasilkomUI/prices-2/ifml-pwa-transformation-tool. 
You can download the source code as a ZIP file and then extract the source code or clone the repository to your computer.

# Environment Preparation
1. Create Virtual Environment:
- Make sure that you have installed PIP (Package Manager for Python)
- Open terminal or command prompt, go to IFML Angular Generator source code (ifml-pwa-transformation-tool) directory.
- Install module virtual environment: `pip install virtuaenv`
- Create virtual environment: `virtualenv venv`

2. Activate Virtual Environment
Windows: `venv\Scripts\activate.bat`
Ubuntu or MacOS: `venv\Scripts\activate.bat`
*This step is always required before running the IFML to Angular Generator*

3. Install Python dependencies:
`pip install -r requirements.txt`

# Generate Product
1. Make sure that the virtual environment is activated

2. Create a json mapping file that consists of product information,
see `CharitySchool.json` for example. 
Fill in the product name, selected features, and other fields.

3. Run the following commands to generate product: 
`python __init__.py <file JSON mapping> <react/angular>`
For example, to generate Angular project for CharitySchool product, run:
`python __init__.py CharitySchool.json angular`

4. The Angular source code is generated in the directory `result\<Product Name>`,
for example Charity School product is generated in the directory `result\Charity School`.

# Run Product
1. Open terminal, go to directory `result <Product Name>` that contains the generated JavaScript
2. Install required modules: `npm install`
3. Run the project: `npm run start`








